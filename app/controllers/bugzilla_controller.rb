class BugzillaController < ApplicationController
  def graphs
    @bugstats = BugStat.order(:date)
    @last_stat = @bugstats.last
    @closed_bug_stats = ClosedBugStat.order(:date)
    @closed_last_stat = @closed_bug_stats.last
  end

  def summary
    @important_bugs = Bug.where("priority <= 2").order(:priority)
    @p5_bugs = Bug.where(priority: 5)
    @y2maint_bugs = Bug.where(assignee: "yast2-maintainers@suse.de").order(:priority)
    @latest_bugs = Bug.order(bug_id: :desc).first(10)
    @last_modified = Bug.order(change_date: :desc).first(10)
    @stale_bugs = Bug.where("status == 'IN_PROGRESS' and change_date <= ?", Date.current - 1.month)
  end

  def index
    @bugs = Bug.order(:priority)
  end
end
