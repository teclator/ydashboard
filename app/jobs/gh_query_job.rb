
require "ydashboard/gh_issues_query"
require "ydashboard/gh_issues_importer"

class GhQueryJob < ApplicationJob
  queue_as :default

  def perform(*args)
    gh_query = Ydashboard::GhIssuesQuery.new
    import_data(gh_query.run)
  ensure
    # enqueue itself
    GhQueryJob.set(wait: 1.hour).perform_later
  end

private

  def import_data(data)
    importer = Ydashboard::GhIssuesImporter.new(data)
    importer.import
  end

end
