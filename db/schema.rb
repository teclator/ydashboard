# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161117204221) do

  create_table "bug_stats", force: :cascade do |t|
    t.date     "date"
    t.integer  "p0"
    t.integer  "p1"
    t.integer  "p2"
    t.integer  "p3"
    t.integer  "p4"
    t.integer  "p5"
    t.integer  "fresh_new"
    t.integer  "confirmed"
    t.integer  "in_progress"
    t.integer  "reopened"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "bugs", force: :cascade do |t|
    t.integer  "bug_id"
    t.integer  "priority"
    t.string   "product"
    t.string   "assignee"
    t.string   "status"
    t.string   "summary"
    t.datetime "change_date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "closed_bug_stats", force: :cascade do |t|
    t.date     "date",                         null: false
    t.integer  "fixed",            default: 0, null: false
    t.integer  "resolved_invalid", default: 0, null: false
    t.integer  "wontfix",          default: 0, null: false
    t.integer  "noresponse",       default: 0, null: false
    t.integer  "upstream",         default: 0, null: false
    t.integer  "feature",          default: 0, null: false
    t.integer  "duplicate",        default: 0, null: false
    t.integer  "worksforme",       default: 0, null: false
    t.integer  "moved",            default: 0, null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["date"], name: "index_closed_bug_stats_on_date"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "gh_issues", force: :cascade do |t|
    t.integer  "number",      null: false
    t.boolean  "is_pull",     null: false
    t.string   "title",       null: false
    t.datetime "modified_at", null: false
    t.integer  "gh_repo_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["gh_repo_id"], name: "index_gh_issues_on_gh_repo_id"
    t.index ["is_pull"], name: "index_gh_issues_on_is_pull"
    t.index ["modified_at"], name: "index_gh_issues_on_modified_at"
  end

  create_table "gh_orgs", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_gh_orgs_on_name"
  end

  create_table "gh_repos", force: :cascade do |t|
    t.string   "name",       null: false
    t.integer  "gh_org_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gh_org_id"], name: "index_gh_repos_on_gh_org_id"
    t.index ["name"], name: "index_gh_repos_on_name"
  end

  create_table "jenkins_statuses", force: :cascade do |t|
    t.string   "name",       null: false
    t.boolean  "internal",   null: false
    t.boolean  "success",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["internal"], name: "index_jenkins_statuses_on_internal"
    t.index ["name"], name: "index_jenkins_statuses_on_name"
  end

  create_table "obs_projects", force: :cascade do |t|
    t.string   "name"
    t.boolean  "internal",   null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_obs_projects_on_name"
  end

  create_table "obs_repo_statuses", force: :cascade do |t|
    t.integer  "obs_project_id"
    t.string   "name"
    t.integer  "failures",       default: 0, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["name"], name: "index_obs_repo_statuses_on_name"
    t.index ["obs_project_id"], name: "index_obs_repo_statuses_on_obs_project_id"
  end

end
