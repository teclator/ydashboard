class CreateGhOrgs < ActiveRecord::Migration[5.0]
  def change
    create_table :gh_orgs do |t|
      t.string :name,  null: false

      t.timestamps
    end
    add_index :gh_orgs, :name
  end
end
