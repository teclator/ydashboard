
require "nokogiri"

module Ydashboard
  class ObsResultImporter
    attr_reader :data, :internal_obs
    
    def initialize(query_result, internal_obs)
      @data = query_result
      @internal_obs = internal_obs
    end
    
    def save!
      doc = Nokogiri::XML(data)
      # there should be just one project, but rather expect more projects
      projects = doc.xpath('/resultlist/result/@project').map { |a| a.value }.uniq

      projects.each do |project|
        ObsProject.transaction do
          obs_prj = ObsProject.where(name: project, internal: internal_obs).first_or_create

          # remove all current statuses
          obs_prj.obs_repo_statuses.delete_all

          # agregate repositories for all architectures
          repositories = doc.xpath("/resultlist/result[@project=\"#{project}\"]/@repository").map { |a| a.value }.uniq

          repositories.each do |repo|
            # count the failed packages for all architectures using XPath count() operator
            failures = doc.xpath("count(/resultlist/result[@project=\"#{project}\" and @repository=\"#{repo}\"]/status[@code=\"failed\"])").to_i
            obs_prj.obs_repo_statuses.create(name: repo, failures: failures)
          end
        end
      end
    end

  end
end
