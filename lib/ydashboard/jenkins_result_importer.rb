

module Ydashboard
  class JenkinsResultImporter
    attr_reader :jenkins_query
    
    def initialize(jenkins_query)
      @jenkins_query = jenkins_query
    end
    
    def import
      data = jenkins_query.run
      JenkinsStatus.transaction do
        data.each do |job|
          job_stat = JenkinsStatus.where(name: job["name"], internal: jenkins_query.internal_jenkins).first_or_create
          job_stat.success = job["color"] == "blue"
          job_stat.save!
        end
      end
    end

  end
end
