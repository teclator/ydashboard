
module Ydashboard
  class BugzillaCredentials

    attr_reader :username, :password
    
    def initialize(username = nil, password = nil)
      @username = username
      @password = password
    end

    def empty?
      !username || !password
    end

    def self.load
      if !File.exist?(bugzilla_config_file)
        Rails.logger.warn "Bugzilla config file (#{bugzilla_config_file}) not found"
        return BugzillaCredentials.new
      end

      config = YAML.load_file(bugzilla_config_file)
      BugzillaCredentials.new(config["bugzilla_login"], config["bugzilla_password"])
    end

    def self.bugzilla_config_file
      "#{Rails.root}/config/bugzilla.yml"
    end

  end
end