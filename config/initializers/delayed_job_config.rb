
# do not restart failed jobs, they are started repeatedly anyway
Delayed::Worker.max_attempts = 1

# do not start jobs in testing or when the table does not exist yet (rake db:migration)
if !Rails.env.test? && Delayed::Job.table_exists?
  # clean the queue
  Delayed::Job.delete_all
  # enqueue all
  BugzillaQueryJob.set(wait: 5.seconds).perform_later
  ObsQueryJob.set(wait: 20.seconds).perform_later
  GhQueryJob.set(wait: 40.seconds).perform_later
  BugzillaClosedQueryJob.set(wait: 70.seconds).perform_later
end
